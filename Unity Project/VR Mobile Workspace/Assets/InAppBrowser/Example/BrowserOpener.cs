﻿using UnityEngine;
using System.Collections;

public class BrowserOpener : MonoBehaviour {

	public string pageToOpen = "http://www.google.com.au";

	void Start () {
		OpenBrowser ();
	}

	// check readme file to find out how to change title, colors etc.
	public void OpenBrowser() {
		InAppBrowser.DisplayOptions options = new InAppBrowser.DisplayOptions();
		options.displayURLAsPageTitle = false;
		options.pageTitle = "InAppBrowser example";
		InAppBrowser.OpenURL(pageToOpen, options);
	}
}
