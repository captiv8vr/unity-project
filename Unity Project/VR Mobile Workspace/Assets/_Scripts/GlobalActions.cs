﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GlobalActions : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void QuitApplication ()
	{
		Application.Quit ();
	}

	public void loadScene(int level)
	{
		SceneManager.LoadScene(level);
	}

	public void SwitchTvMode(){
		DefaultLayout.dualTvMode = !DefaultLayout.dualTvMode;
	}


}
