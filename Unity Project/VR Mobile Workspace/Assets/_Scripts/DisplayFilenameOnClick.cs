﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DisplayFilenameOnClick : MonoBehaviour {

	public void DisplayFilename() {
		GameObject[] others = GameObject.FindGameObjectsWithTag ("FileNamePrefab");
		foreach(GameObject img in others) {
			img.GetComponent<Image> ().color = new Vector4 (0.9f,0.9f,0.9f,1f);
		}
		this.GetComponent<Image>().color = new Vector4 (0.75f,0.75f,0.75f,1f);
		GameObject fc = GameObject.FindGameObjectWithTag ("FileController");
		fc.GetComponent<FileController>().SetFileName (this.GetComponentInChildren<Text>().text);
	}
}
