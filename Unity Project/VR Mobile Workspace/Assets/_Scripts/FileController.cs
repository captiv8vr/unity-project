﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;

public class FileController: MonoBehaviour {

	public InputField inputfield;
	public InputField filenameInputField;
	public Image filenamePrefab;
	public RectTransform contentPanel;
	public GameObject inputPanel;
	public GameObject fileListPanel;
	public GameObject saveButton;
	public GameObject loadButton;
	public GameObject inputPanelSaveButton;
	public GameObject inputPanelLoadButton;
	public GameObject inputPanelCancelButton;
	public GameObject popupMessage;

//	string defaultfilename;

	// set the file name input field
	public void SetFileName (String filename) {
		filenameInputField.GetComponent<InputField> ().text = filename;
		// select and highlist the text in the input field
		/*if (filenameInputField.interactable) {
			filenameInputField.ActivateInputField ();
			filenameInputField.GetComponent<InputField> ().selectionAnchorPosition = 0;
			filenameInputField.GetComponent<InputField> ().selectionFocusPosition = filenameInputField.GetComponent<InputField> ().text.Length;
			//filenameInputField.GetComponent<InputField> ().caretPosition = filenameInputField.GetComponent<InputField> ().text.Length;
		}*/
	}

	// switch on and off the file IO panel
	public void PanelMode(Boolean flag) {
		inputPanel.SetActive (flag);
		fileListPanel.SetActive (flag);
		saveButton.SetActive (!flag);
		loadButton.SetActive (!flag);
	}

	// clear filenameInputField e.g. when "cancel" is hit
	public void ClearInputField(InputField filenameInputField) {
		filenameInputField.GetComponent<InputField>().text = "";
	}

	// List files and enable/disable buttons/input fields
	public void FileIOPanel(Boolean isSave) {
		DirectoryInfo dir = new DirectoryInfo(Application.persistentDataPath + "/documents/");
		FileInfo[] info = dir.GetFiles("*.dat");
		// if there's no existing file
		if (info.Length == 0) {
			// when saving file
			if (isSave) {
				PanelMode (true);
			}
			// when loading file
			else {
				// do nothing
			}
		}
		// if there's existing file(s)
		else {
			// clear the list first
			Transform[] children = contentPanel.GetComponentsInChildren<Transform>();
			foreach (Transform child in children) {
				if(contentPanel.transform.IsChildOf(child.transform) == false)
					Destroy (child.gameObject);
			}
			// for each file, create a text object under the content panel
			foreach (FileInfo f in info) {
				Image aFile = (Image)Instantiate (filenamePrefab);
				aFile.GetComponentInChildren<Text>().text = f.Name.Substring(0,f.Name.Length - 4);	// only display file name, for full file name, assign f.Name only
				aFile.rectTransform.SetParent (contentPanel, false);	//transform.SetParent (contentPanel); the argument "false" is important

				//Debug.Log (f.Name);
			}
			PanelMode (true);
		}
		// switch between save button and load button in the input panel
		inputPanelSaveButton.SetActive (isSave);
		inputPanelLoadButton.SetActive (!isSave);
		popupMessage.SetActive (false);
		inputPanelCancelButton.GetComponentInChildren<Text>().text = "Cancel";

		// set input field interactable when saving, vice versa
		filenameInputField.interactable = isSave;

		// set input field place holder text
		if (isSave)
			filenameInputField.GetComponent<InputField> ().placeholder.GetComponent<Text> ().text = "Enter file name...";
		else
			filenameInputField.GetComponent<InputField>().placeholder.GetComponent<Text>().text = "Select file...";

		// get recent saved filename
		String filename;
		if(File.Exists(Application.persistentDataPath + "/config/RecentFileName.txt")){
			filename = File.ReadAllText(Application.persistentDataPath + "/config/RecentFileName.txt");
			filenameInputField.GetComponent<InputField>().text = filename;
		}
	}

	public void SaveToFile(){
		String filename = filenameInputField.GetComponent<InputField>().text;
		if (filename.Length == 0) {
			popupMessage.SetActive (true);
			popupMessage.GetComponent<Text> ().text = "Please enter file name.";
			return;
		}
		TextFile textfile = new TextFile ();
		//textfile.filename = filename;	// get filename from prompt
		textfile.contents = inputfield.GetComponent<InputField>().text;	// get contents from text editor
			
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream fs = File.Create (Application.persistentDataPath + "/documents/" + filename + ".dat");	// create file as /filename.dat

		bf.Serialize (fs, textfile);	// write to file
		fs.Close ();

		// save the filename as recent saved filename
		File.WriteAllText(Application.persistentDataPath + "/config/RecentFileName.txt", filename);
		popupMessage.SetActive (true);
		popupMessage.GetComponent<Text> ().text = "File Saved Successfully.";
		inputPanelCancelButton.GetComponentInChildren<Text>().text = "Done";
	}

	public void LoadFromFile(){
		String filename = filenameInputField.GetComponent<InputField>().text;
		if(File.Exists(Application.persistentDataPath + "/documents/" + filename + ".dat")){
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream fs = File.Open (Application.persistentDataPath + "/documents/" + filename + ".dat", FileMode.Open);	// open file named /filename.dat
			try {
				TextFile textfile = (TextFile)bf.Deserialize(fs);	// read into buffer
				fs.Close ();

				inputfield.GetComponent<InputField>().text = textfile.contents;	// load data
			}
			catch(Exception e) {
				inputfield.GetComponent<InputField> ().text = "";
			}
			PanelMode(false);	// go back to edit mode
			File.WriteAllText(Application.persistentDataPath + "/config/RecentFileName.txt", filename);
		}
	}

	void Awake() {
		Directory.CreateDirectory (Application.persistentDataPath + "/config");
		Directory.CreateDirectory (Application.persistentDataPath + "/documents");
		//defaultfilename = GetRecentFileName ();
	}
}

[Serializable]
class TextFile {
//	public string filename;
	public string contents;
}
/*
[Serializable]
class DefaultFileName {
	public string text;
}*/