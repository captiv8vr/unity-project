﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class HotKeys : MonoBehaviour {
	public FileController fc;
	public GameObject textEditor;
	public GameObject fileListPanel;
	public GameObject filenameInputFieldSaveButton;
	public GameObject filenameInputFieldLoadButton;
	public GameObject filenameInputFieldCancelButton;
	public GameObject webBroswerScreen;
	public GameObject constantScreenWebBrowser;
	public GameObject UninteractiveScreenWebBrowser;

	void Update () {
		// accept hot keys below only if text editor is active
		if (textEditor.activeSelf && !fileListPanel.activeSelf) {
			// lctrl + S = Save
			if (Input.GetKey (KeyCode.LeftControl) && Input.GetKey (KeyCode.S)) {
				fc.FileIOPanel (true);	// go to save file panel
			}
			// lctrl + L = Load
			if (Input.GetKey (KeyCode.LeftControl) && Input.GetKey (KeyCode.L)) {
				fc.FileIOPanel (false);	// go to load file panel
			}
		}
		if (fileListPanel.activeSelf) {
			// press enter to quickly save/load file when in filechoosing panel
			if (Input.GetKey (KeyCode.Return)) {
				if (filenameInputFieldSaveButton.activeSelf) {
					fc.SaveToFile();
				}
				if (filenameInputFieldLoadButton.activeSelf) {
					fc.LoadFromFile();
				}
			}
			// press esc to quickly exit save/load file panel
			if (Input.GetKey (KeyCode.Escape)) {
				if (filenameInputFieldCancelButton.activeSelf) {
					fc.PanelMode(false);
				}
			}
		}
		// hide the webview when esc is pressed
		if (this.GetComponent<WebViewLauncher>().GetVisibility()) {
			if (Input.GetMouseButton(0)) {
				UninteractiveScreenWebBrowser.SetActive (false);
				constantScreenWebBrowser.SetActive (true);
//				GameObject.FindGameObjectWithTag ("Calendar").SetActive(false);
				this.GetComponent<WebViewLauncher> ().SetVisibility(false);
				//GameObject.Destroy (webBroswerScreen);
				//webBroswerScreen.GetComponent<WebViewObject> ().SetVisibility (false);
				//webBroswerScreen.SetActive (false);
			}
		}
	}
}
