﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DetectTvMode : MonoBehaviour {
	public GameObject dualTvModeButton;
	public void UpdateDualTvModeText() {
		if (DefaultLayout.dualTvMode) {
			dualTvModeButton.GetComponentInChildren<Text> ().text = "DUAL TV: ON";
		} else {
			dualTvModeButton.GetComponentInChildren<Text> ().text = "DUAL TV: OFF";
		}
	}
	// Use this for initialization
	void Start () {
		UpdateDualTvModeText ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
