/*
 * Copyright (C) 2012 GREE, Inc.
 * 
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 * 
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 * 
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

using System.Collections;
using UnityEngine;

public class WebViewLauncher : MonoBehaviour
{
    public string Url = "http://www.llluper.website";
//    public GUIText status;
    WebViewObject webViewObject;
	public int topBottomScale = 5;
	public int leftRightScale = 10;
	bool visible = false;
//	public bool isLeft;

	// added by Ty Zhang
	// function that takes a boolean and sets the visibility of the web browser accordingly
	public void SetVisibility(bool show) {
		if (webViewObject != null) {
			webViewObject.SetVisibility (show);
			visible = show;
		}
	}

	// added by Ty Zhang
	public bool GetVisibility() {
		return visible;
	}

	// modified by Ty such that the setMargins function takes scales rather than fixed number pixels
    IEnumerator Start()
    {
//		if (webViewObject == null) {
			webViewObject = (new GameObject ("WebViewObject")).AddComponent<WebViewObject> ();
			webViewObject.Init (
				enableWKWebView: true);
			// set the web broswer screens according to the scale set in Unity
			webViewObject.SetMargins (leftRightScale, topBottomScale, leftRightScale, topBottomScale);
			
			//webViewObject.SetVisibility (true);

#if !UNITY_WEBPLAYER
			if (Url.StartsWith ("http")) {
				webViewObject.LoadURL (Url.Replace (" ", "%20"));
			} else {
				var exts = new string[] {
					".jpg",
					".html"  // should be last
				};
				foreach (var ext in exts) {
					var url = Url.Replace (".html", ext);
					var src = System.IO.Path.Combine (Application.streamingAssetsPath, url);
					var dst = System.IO.Path.Combine (Application.persistentDataPath, url);
					byte[] result = null;
					if (src.Contains ("://")) {  // for Android
						var www = new WWW (src);
						yield return www;
						result = www.bytes;
					} else {
						result = System.IO.File.ReadAllBytes (src);
					}
					System.IO.File.WriteAllBytes (dst, result);
					if (ext == ".html") {
						webViewObject.LoadURL ("file://" + dst.Replace (" ", "%20"));
						break;
					}
				}
			}
#if !UNITY_ANDROID
			webViewObject.EvaluateJS (
				"window.addEventListener('load', function() {" +
				"   window.Unity = {" +
				"       call:function(msg) {" +
				"           var iframe = document.createElement('IFRAME');" +
				"           iframe.setAttribute('src', 'unity:' + msg);" +
				"           document.documentElement.appendChild(iframe);" +
				"           iframe.parentNode.removeChild(iframe);" +
				"           iframe = null;" +
				"       }" +
				"   }" +
				"}, false);");
#endif
#else
        if (Url.StartsWith("http")) {
            webViewObject.LoadURL(Url.Replace(" ", "%20"));
        } else {
            webViewObject.LoadURL("StreamingAssets/" + Url.Replace(" ", "%20"));
        }
        webViewObject.EvaluateJS(
            "parent.$(function() {" +
            "   window.Unity = {" +
            "       call:function(msg) {" +
            "           parent.unityWebView.sendMessage('WebViewObject', msg)" +
            "       }" +
            "   };" +
            "});");
#endif
			//					webViewObject.SetMirror (Screen.width / leftRightScale, Screen.height / topBottomScale, Screen.width / 2 + Screen.width / leftRightScale, Screen.height / topBottomScale);
			yield break;
//		} else {
//			webViewObject.SetVisibility (true);
//		}
	}
#if !UNITY_WEBPLAYER
    void OnGUI()
    {
    }
#endif
}
