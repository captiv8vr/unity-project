﻿using UnityEngine;
using System.Collections;

public class TestURL : MonoBehaviour {

	public string url = "https://www.google.com.au";
	IEnumerator Start() {
		WWW www = new WWW(url);
		yield return www;
		Renderer renderer = GetComponent<Renderer>();
		renderer.material.mainTexture = www.texture;
	}
}
