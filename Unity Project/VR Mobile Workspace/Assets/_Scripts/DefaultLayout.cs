﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DefaultLayout : MonoBehaviour {

	public static bool dualTvMode = false;
	public GameObject tv;
	public GameObject constantScreen;
	public GameObject textEditor;
	public GameObject webBrowserScreen;
	public GameObject dualTv;
	public GameObject constantScreen1;
	public GameObject textEditor1;
	public GameObject constantScreen2;
	public GameObject textEditor2;
	//public InputField inputField;
	//public InputField fileNameInputField;
	public void DualTvMode(bool dualTvMode) {
		if (dualTvMode) {
			tv.SetActive (false);
			dualTv.SetActive (true);
			constantScreen1.SetActive (true);
			textEditor1.SetActive (false);
			constantScreen2.SetActive (true);
			textEditor2.SetActive (false);
		} else {
			tv.SetActive (true);
			dualTv.SetActive (false);
			constantScreen.SetActive (true);
			textEditor.SetActive (false);
			webBrowserScreen.SetActive (false);
		}


	}

	// Use this for initialization
	void Start() {
			DualTvMode (dualTvMode);
		}

		//TouchScreenKeyboard.hideInput = true;

		// disable mobile input
		//inputField.keyboardType = (TouchScreenKeyboardType)(-1);
		//fileNameInputField.keyboardType = (TouchScreenKeyboardType)(-1);
	/*
	void OnGUI() {
		// hide mobile input
		TouchScreenKeyboard.hideInput = true;
	}*/
}
